var handleRightArrow = function (aBoard) {
    return handleLeftArrow(aBoard.reverseRows()).reverseRows();
};
var handleLeftArrow = function (aBoard) {
    return aBoard.reduce(function (newBoard, row) {
        var canMerge = true;
        var newRow = row.reduce(function (acc, next) {
            if (next === null) {
                return acc;
            }
            if (canMerge && acc.length > 0 && next.value === acc[acc.length - 1].value) {
                var mergeWithTile = acc[acc.length - 1];
                mergeWithTile.value *= 2;
                mergeWithTile.mergeWith(next.id);
                canMerge = false;
            } else {
                acc.push(next);
                canMerge = true;
            }
            return acc;
        }, []);
        var missingPadding = 4 - newRow.length;
        for (var i = 0; i < missingPadding; i++) {
            newRow.push(null);
        }
        newBoard.push(newRow);
        return newBoard;
    }, []);
};
var handleUpArrow = function (aBoard) {
    return handleLeftArrow(aBoard.rowsToColumns()).rowsToColumns();
};
var handleDownArrow = function (aBoard) {
    return handleRightArrow(aBoard.rowsToColumns()).rowsToColumns();
};
var id_provider = 1;
var LEFT_ARROW_KEY_CODE = 37;
var UP_ARROW_KEY_CODE = 38;
var RIGHT_ARROW_KEY_CODE = 39;
var DOWN_ARROW_KEY_CODE = 40;
function handleArrowPress(keyCode, board) {
    var dir;
    var isKeyCodeForArrow = true;
    switch (keyCode) {
        case LEFT_ARROW_KEY_CODE:
            board = handleLeftArrow(board);
            dir = 'left';
            break;
        case RIGHT_ARROW_KEY_CODE:
            board = handleRightArrow(board);
            dir = 'right';
            break;
        case UP_ARROW_KEY_CODE:
            board = handleUpArrow(board);
            dir = 'left';
            break;
        case DOWN_ARROW_KEY_CODE:
            board = handleDownArrow(board);
            dir = 'right';
            break;
        default:
            isKeyCodeForArrow = false;
            break;
    }
    if (isKeyCodeForArrow) {
        $('body').addClass('animating-'+board.name);
        update(dir, board, false, function () {
            board.populateRandomOpenCell(id_provider++, Math.floor(Math.random() * 2) + 1);
            update('left', board, true);
        });
    }
    return board;
}
function updateColourDisplayer(red, green, blue) {
    var rgb = 'RGB(' + red + ',' + green + ',' + blue + ')';
    $('.colour-display').css('background-color', rgb);
    $('#colour-display-label')[0].innerHTML = "Displaying " + rgb;
}
var arrowPressHandler = function (e) {
    var body = $('body');
    if (body.hasClass('animating-'+redBoard.name)
        || body.hasClass('animating-'+blueBoard.name)
        || body.hasClass('animating-'+greenBoard.name)) {
        return;
    }
    var keyCode = e.keyCode;
    redBoard = handleArrowPress(keyCode, redBoard);
    greenBoard = handleArrowPress(keyCode, greenBoard);
    blueBoard = handleArrowPress(keyCode, blueBoard);
    var red = redBoard.getValue();
    var green = greenBoard.getValue();
    var blue = blueBoard.getValue();
    updateColourDisplayer(red, green, blue);
};
function Tile(id, value) {
    this.id = id;
    this.value = value;
    this.mergeWithId = null;
}

Tile.prototype.mergeWith = function(id) {
    this.mergeWithId = id;
};

function Board(name, size, board) {
    this.name = name;
    this.size = size;
    if (board == null || board.length != size) {
        this.board = createEmptyBoard(size);
    } else {
        this.board = board;
    }

    function createEmptyBoard(size) {
        var board = [];
        for (var i = 0;  i < size; i++) {
            var newRow = [];
            for (var j = 0; j < size; j++) {
                newRow.push(null);
            }
            board.push(newRow);
        }
        return board;
    }
}

Board.prototype.populateRandomOpenCell = function(tileId, initialValue) {
    var emptyCellCount = this.getEmptyCellCount();
    var index = Math.floor(Math.random() * emptyCellCount) + 1;
    var cellsToSkip = index - 1;
    row_loop:
        for (var i = 0; i < this.size; i++) {
            for (var j = 0; j < this.size; j++) {
                if (this.board[i][j] == null) {
                    if (--cellsToSkip <= 0) {
                        this.board[i][j] = new Tile(tileId, initialValue);
                        break row_loop;
                    }
                }
            }
        }
};
Board.prototype.getEmptyCellCount = function() {
    return this.board.reduce(function(count, newRow) {
        return newRow.reduce(function (rowCount, val) {
            if (val == null) {
                return rowCount + 1;
            }
            return rowCount;
        }, count);
    }, 0)
};

Board.prototype.forEachCell = function(dir, cellCallback) {
    var callBackApplier;
    var self = this;
    switch (dir) {
        case 'left':
            callBackApplier = function(brd, row, col) {
                cellCallback(self.board[row][col], row, col);
            };
            break;
        case 'right':
            callBackApplier = function (brd, row, col) {
                cellCallback(self.board[self.size - 1 - row][self.size - 1 - col], self.size - 1 - row, self.size - 1 - col);
            };
            break;
        default:
            throw new Error("unrecognized direction: " + dir);
    }
    for (var i = 0; i < this.size; i++) {
        for (var j = 0; j < this.size; j++) {
            callBackApplier(this.board, i, j);
        }
    }
};

Board.prototype.reduce = function(reducer, init) {
    var newBoard = this.board.reduce(reducer, init);
    return new Board(this.name, this.size, newBoard);
};

Board.prototype.getValue = function() {
    var valCounts = {};
    this.forEachCell('left', function (tile) {
        if (tile) {
            var valCount = valCounts[tile.value];
            if (!valCount) valCount = 0;
            valCounts[tile.value] = valCount + 1;
        }
    });
    var sum = 0;
    for (val in valCounts) {
        sum  += val * (valCounts[val] % 2)
    }
    return sum % 256;
};

Board.prototype.rowsToColumns = function() {
    var boardAsColumns = [];
    for (var i = 0; i < this.board.length; i++) {
        boardAsColumns.push(this.board.map(function (row) {
            return row[i];
        }));
    }
    return new Board(this.name, this.size, boardAsColumns);
};

Board.prototype.reverseRows = function() {
    var reversedBoard = [];
    this.board.forEach(function (row) {
        reversedBoard.push(row.slice().reverse());
    });
    return new Board(this.name, this.size, reversedBoard);
};
